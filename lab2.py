import math

#1b
def media(a,b,c):
	"""informar 3 valores"""
	return (a+b+c)/3


#1c
def media_1(a,b,c):
	"""informar 3 valores, o resultado é o maior número menos a média"""
	
	return max(a,b,c) - media(a,b,c)


#1d
def media_2(a,b,c):
	"""informar 3 valores, o resultado é o menor número menos a média"""

	return min(a,b,c) + media(a,b,c) 


#2a
def discriminante(a,b,c):
	"""informar a, b e c para cálculo do discriminante"""
	return b**2 - 4 * a * c


#2b
def raiz_real_1(a,b,c):
	"""informar a, b e c para cálculo da primeira raíz"""
	return  (b * -1 + math.sqrt(discriminante(a,b,c))) / (2 * a)


#2c
def raiz_real_2(a,b,c):
	"""informar a, b e c para cálculo da primeira raíz"""
	return  (b * -1 - math.sqrt(discriminante(a,b,c))) / (2 * a)


#3b
def ntermos_pa(a1,an,r):
	"""informar a1, an e r para achar n termos"""
	return (an - a1 + r)/r

	
#3c
def soma_pa(a1,an,r):
	"""informar a1, an e r para achar a soma de n termos"""

	return ((a1 + an) * ntermos_pa(a1,an,r)) / 2 


#4a
def dist_pontos(xb,yb,xa,ya):
	"""informar na ordem as coordenadas xb, yb, xa, ya para achar a distância entre pontos"""
	return math.sqrt((xb - xa)**2 + (yb - ya)**2) 


#4b
def perimetro_triangulo(xv1,yv1,xv2,yv2,xv3,yv3):
	"""informar pontos dos vértices do triângulo"""
	
	return dist_pontos(xv1,yv1,xv2,yv2) + dist_pontos(xv2,yv2,xv3,yv3) + dist_pontos(xv3,yv3,xv1,yv1)


#4c
def produto_sen_cos(ang):
	"""informar o ângulo"""
	return math.sin((ang/180) * 3.1415)**2 + math.cos((ang/180) * 3.1415)**2

	
#5
def area_setor(r, m=360):
	"""informar raio e ângulo central"""
	return (m / 360) * (3.1415 * r ** 2)  









	


